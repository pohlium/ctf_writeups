This challege only word with the web shell.  
We log into the shell, and navigate to the path of our probem.  
Here we find a folder called "files" with multiple files, and we have to find
the flag.  
grep has a feature to recursively look through all files in a folder, so we use
that to grep out our flag: `grep -r pico files`: `picoCTF{grep_r_and_you_will_find_24c911ab}`
