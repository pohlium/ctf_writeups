In this challenge, we get a linux `passwd` and `shadow` file, and a connect
command for a remote server: `nc 2018shell.picoctf.com 38860`.  
We have to extract the username and password from the given files, to login to
the server.  
The contents of the files are:  
```
/* passwd */
root:x:0:0:root:/root:/bin/bash

/* shadow */
root:$6$IGI9prWh$ZHToiAnzeD1Swp.zQzJ/Gv.iViy39EmjVsg3nsZlfejvrAjhmp5jY.1N6aRbjFJVQX8hHmTh7Oly3NzogaH8c1:17770:0:99999:7:::
```  
The username is easy, we just have one user called `root`.  
The password is a little more difficult, as we only have a salted and hashed
form of it in the shadow file.  
However, there is a tool called `john` or `john the ripper` that we can use to
crack this password.  
First, we have to combine the passwd and shadow files to one file in the old
passwd format.  
We do this with the command `unshadow passwd shadow > outfile`  
This gives us the following file:  
```
/* outfile */
root:$6$IGI9prWh$ZHToiAnzeD1Swp.zQzJ/Gv.iViy39EmjVsg3nsZlfejvrAjhmp5jY.1N6aRbjFJVQX8hHmTh7Oly3NzogaH8c1:0:0:root:/root:/bin/bash
```
We can now crack this with john: `john outfile` and it will quickly return the
result: `password1`.  

Now we log into the server with the username `root` and the password
`password1` to get the flag: `picoCTF{J0hn_1$_R1pp3d_4e5aa29e}`
