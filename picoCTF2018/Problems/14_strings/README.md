In this challenge we get a file called "strings" and we have to get the flag
without running the file.  
We get it with the command `strings strings | grep pico`: `picoCTF{sTrIngS_sAVeS_Time_3f712a28}`
