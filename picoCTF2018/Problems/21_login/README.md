In this challenge we are supposed to log into a page as admin.  
We can log in as any other user, but not as admin.  
The way the login works, is that the website sets a cookie. The cookie looks
something like this: `"password=1234; username=asdfg; admin=False"`  
Now we just want to set the admin flag to True by going into the dev tools of
our browser, and typing `document.cookie="admin=True"`. The we reload the page
and we get our flag: `picoCTF{l0g1ns_ar3nt_r34l_a280e12c}`
