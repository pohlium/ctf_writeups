In this challenge we are asked to connect to the server `2018shell.picoctf.com`
on port `36356`.  
We do that by typing `nc 2018shell.picoctf.com 36356` and get the flag
`picoCTF{NEtcat_iS_a_NEcESSiTy_9454f3e0}`.  
Keep in mind that your netcat executable is not always called `nc`, sometimes
its just called `netcat`.  

