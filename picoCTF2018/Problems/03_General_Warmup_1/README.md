In this challenge, were asked what the hex number `0x41` would be in ASCII.  
There are many ways to solve it. Genereally, i can only recommend you to learn by heart that 0x41 is an `A` and 0x61 is an `a`, that is often pretty helpful to notice text in random hex. However, if you wanna solve this and dont know the solution by heart there are lots of ways.  
One easy ways is to look up an ascii table and just look at what 0x41 corresponds to. Another way thats a bit more programmer like is to fire up python and do
```py
print(chr(0x41))
```
which will also return you `'A'`.  
Now we just convert it to the usual flag format and we get `picoCTF{A}` as our flag.