In this challenge, we get a connect to `2018shell.picoctf.com 10493` and a file
called `incident.json` that contains 10 "tickets".  
Because were cool, we will build a python script that answers all the
questions.  

First of all, lets build a frame that parses the json:

```py
import json

def main():
    with open("incidents.json", 'r') as fp:
        tickets = json.load(fp)

if __name__ == "__main__":
    main()
```

Now we will build functions for each question.  

### Question 1:
"What is the most common source IP address?"  

```py
def q1(tickets):
    cnt = collections.Counter()
    for ticket in tickets["tickets"]:
        cnt[ticket["src_ip"]] += 1
    return cnt.most_common(1)[0][0]
```

returns: 251.71.156.29

### Question 2:
"How many unique destination IP addresses were targeted by the source IP address 31.13.251.15?"  
(The IP address may vary)  
```py
def q2(tickets, ip):
    dest = set()
    for ticket in tickets["tickets"]:
        if ticket["src_ip"] == ip:
            dest.add(ticket["dst_ip"])
    return len(dest)
```

returns: 2 (result may vary depending on the ip address in your question.)

### Question 3:
"What is the number of unique destination ips a file is sent, on average? Needs to be correct to 2 decimal places."  

```py
def q3(tickets):
    file_dest_pairs = set()
    for ticket in tickets["tickets"]:
        file_dest_pairs.add((ticket["file_hash"], ticket["dst_ip"]))
    cnt = collections.Counter()
    for pair in file_dest_pairs:
        cnt[pair[0]] += 1
    return sum(cnt.values())/len(cnt)
```

returns: 1.125


If we correctly solve all 3 questions, we get the flag: `picoCTF{J4y_s0n_d3rUUUULo_a062e5f8}`
