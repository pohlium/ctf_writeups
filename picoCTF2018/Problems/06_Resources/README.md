This is not really a challenge, but a great way to get people to look at the resources page.  
You should really look into the guides as a beginner, it is well written and pretty interesting!  
You can get there via this link: [https://picoctf.com/resources](https://picoctf.com/resources)