Our task is to convert a hex number `0x3D` to a base10 number.  
Again im using python for this:
```py
print(0x3D)
```
which returns 61. This results in our flag being `picoCTF{61}`