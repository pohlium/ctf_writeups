Download the zip, unpack the zip, see that `rand2` is a Binary, do `strings
rand2 | grep CTF` on it and get the flag: `CTF{welcome_to_googlectf}.
