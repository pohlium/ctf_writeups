start the `init_sat` program. It establishes an internet connection and gets
data from the internet.

## get the hint

if you dont know what to do rn, you enter the name `Osmium`.  
In the next menu, you select (a) "display config data".  
This provides you with some useless login data, and a link to a google doc.  
in the doc you find the text:
```
VXNlcm5hbWU6IHdpcmVzaGFyay1yb2NrcwpQYXNzd29yZDogc3RhcnQtc25pZmZpbmchCg==
```

This is base64 encoded, decoded it gives you:
```
Username: wireshark-rocks
Password: start-sniffing!
```

Now you realize you have to sniff with wireshark.  
Start it up and use the `init_sat` program again. after connecting to the
Osmium and requesting the config data again, you now see the password in
cleartext in your wireshark packet: 
`CTF{4efcc72090af28fd33a2118985541f92e793477f}`
