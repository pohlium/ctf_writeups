We are told that there isa flag in a file, so we go there on the shell server.  
We know the flag starts with `picoCTF` so we just grep the file contents for
that.  
```
cd /problems/first-grep_5_452e1c1630eb14b6753e9a155c3ae588
cat file | grep picoCTF
```
this returns the flag: `picoCTF{grep_is_good_to_find_things_887251c6}`
