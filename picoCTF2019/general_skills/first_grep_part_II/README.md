We go to the problem folder and see that we have a folder called `files` with 9
folders inside, which each has multiple files inside.  
To get the flag we want to grep throughout all the files, which is easily done
like this:
```
grep -R picoCTF .
```
This gives us the flag: `picoCTF{grep_r_to_find_this_0898e9c9}`
