"What is 0x3D (base 16) in decimal (base 10)?"
```py
print(0x3D)
```
->61
--> Flag = `picoCTF{61}`
