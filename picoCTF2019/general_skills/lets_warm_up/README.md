"If i told you a word would start with 0x70 in hex, what letter would it start
with in ASCII?"

```py
print(char(0x70))
```

-> Flag is `picoCTF{p}`
