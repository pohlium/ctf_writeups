Use the tool `strings` to get all the strings out of the binary file, then grep
for the flag:
```bash
strings strings | grep picoCTF
```
gives us: `picoCTF{5tRIng5_1T_dd210c06}`
