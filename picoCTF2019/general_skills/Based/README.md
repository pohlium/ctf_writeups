```py
msg = input("Your message: ")
print("".join([chr(int(x, 2)) for x in msg.split(' ')]))

msg = input("Your message: ")
print("".join([chr(int(x, 8)) for x in msg.split(' ')]))

msg = input("Your message: ")
print(bytes.fromhex(msg))
```

-> flag = `picoCTF{learning_about_converting_values_d57e7f86}`