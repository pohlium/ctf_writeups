This is just supposed to make us look into how to use netcat.  
For me netcat is called with `nc` so the command to connect is
```bash
nc 2019shell1.picoctf.com 21865
```
This gives us the flag: `picoCTF{nEtCat_Mast3ry_4fefb685}`
