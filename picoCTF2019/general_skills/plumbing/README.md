We want to redirect the output of netcat to a file like this:
```
nc 2019shell1.picoctf.com 13203 > file.txt 2>&1
```

Then we look for the flag inside the file:
```
cat file.txt | grep picoCTF
```
and get the flag: `picoCTF{digital_plumb3r_995d3c81}`
