We are given a file `flag.zip` and we just have to unzip and look at the
picture inside.  
You can unzip the file with  
```bash
unzip flag.zip
```

The flag is: `picoCTF{unz1pp1ng_1s_3a5y}`
