You have the file `flag.txt` but it is not a txt file.  
With the tool `file` we can get the true filetype of the file:
`file flag.txt` -> PNG
now we rename the file to `flag.png` and open it with an image viewer and we
get the flag -> `picoCTF{now_you_know_about_extensions}`
