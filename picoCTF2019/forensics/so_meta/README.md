You have a picture, and the flag is in the "artist" metadata of it
you can view it with any program or website that can display exif data of a
picture.  
-> flag is `picoCTF{s0_m3ta_3d6ced35}`

you can also do it with `exiftool pico.img` or `strings pico.img | grep pico`