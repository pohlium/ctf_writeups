We got the file `captured.pcap` and open it in wireshark.  
Next, we look through the first few packets and notice that some of these kind
of spell out `picoCTF`, so we click "follow udp stream", which applies
`udp.stream eq 7` as a filter. We then get a flag displayed:
`picoCTF{N0t_a_flag}`  
However this is not the real flag.
We play around with the streams a bit more nad display stream 6 next, and we
get the real flag: `picoCTF{StaT31355_636f6e6e}`




NetworkMiner is also a good tool (free version epical!)