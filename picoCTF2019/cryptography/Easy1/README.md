We know that we need to solve a one time pad with the ciphertext `UFJKXQZQUNB`
and the key `SOLVECRYPTO`.
This is a short python script to solve it:
```py
msg = "UFJKXQZQUNB"
key = "SOLVECRYPTO"
print("".join([chr(((ord(letter) - ord(key[i]) - 130) % 26) + 65)
               for i, letter in enumerate(msg)]))
```
which gives us the result `CRYPTOISFUN` and thus the flag `picoCTF{CRYPTOISFUN}`
