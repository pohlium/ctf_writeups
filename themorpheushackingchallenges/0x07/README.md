# [Challenge 0x07](https://www.youtube.com/watch?v=uRi73I7SoNA)
In this challenge we get two different addresses, one that leads us to the
login page, and one called "shell".  
The login page can be accessed via the browser, but it doesnt have anything
interesting in it.  
The shell address can not simply be accessed via the browser through http, so
we assume that it is something different.  
After trying some protocols, you will find out that it is a telnet connection.  
Here is a little python script to get the password:

```py
from telnetlib import Telnet
with Telnet("185.244.192.170", 20007) as tn:
    buf = tn.read_some()
print(buf)
```

This script prints out the password we can put into the login page and we will
receive our flag.
