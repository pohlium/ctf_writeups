# [Challenge 0x06](https://www.youtube.com/watch?v=yRAwZuN3s7A)
This challenge is very simple, a quick look at the source code exposes a comment to look at the file `index.txt`.
Doing so reveals source code of another HTML page with the flag in it.