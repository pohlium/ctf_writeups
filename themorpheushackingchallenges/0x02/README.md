# [Challege 0x02](https://www.youtube.com/watch?v=w0QjwHlxtRs)
Viewing the page source we see pretty much nothing but a standard html form.  
We just try the easiest possible attack (except for bruteforcing regarding simplicity) that everyone should know, a simple SQL injection.  
We know that the username for the challenges will always be `admin`, thus we will try an injection on the password parameter.  
Our payload will be the following:

```SQL
' OR '1'='1
```

We can assume that the password will be enclosed in quotes, thus we close them to inject our own SQL. Since we know there will be another quote coming (the original close quote) we will need to keep that in mind for our comparison.  
We compare the string `'1'` to itself, but on the second one we leave the closing quote away because the original closing quote will close it.  
With this SQL injection we created a tautology, a statement that will always return true. Thus our login is evaluated as correct and we receive the flag.