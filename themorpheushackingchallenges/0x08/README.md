# [Challenge 0x08](https://www.youtube.com/watch?v=P0k088HdxfY)
We see an unusual encounter here, a download link. Upon clicking it we just receive a useful (yet arguably funny) textdocument.
Investigating the source code we see that the download link actually links to `download.php?path=chall.txt`.  
Sadly the download.php is restricted, but we can still download the `index.php` by visiting `download.php?path=index.php`.  
If we look at the now downloaded php file we can quickly find our flag.
