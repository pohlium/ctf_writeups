# [Challenge 0x03](https://www.youtube.com/watch?v=xYQA8tcRpPo)
Looking at the sourcecode of the website, we can see that there is a submit() function that gets triggered upon pressing the `like` button.  
Investigating that function a little further we see that the function creates a new form with an input field with the name `topic` and the value `mynuke` .  
If we press it we will notice that in fact a POST request with `topic=mynuke` gets sent to the server.  
From the last challenge we already know basic SQL injection, however we can try as much as we want, the top form is not injecteable.  
But maybe the hidden one is? Let's try it out:

```bash
sqlmap -u http://185.244.192.170:20002/index.php --level=5 --risk=3 --method POST --data="topic=mynuke" -p "topic" --dbms=MySQL --dbs --tables
```
(the IP might have changed by the time youre reading this)  
`sqlmap` is the program we use to automatically try various SQL injections and also give us some info about the DBs.  
`-u http://185.244.192.170:20002/index.php` defines what IP we want to scan.  
`--level 5 --risk 3` defines what kind of attacks we want to perform, this is the maximum number of attacks since we dont have to be careful in any way.  
`--method POST` defines that we wanna send POST requests.  
`--data="topic=mynuke"` defines what datastring we want to send.  
`-p "topic"` defines the parameter we want to test for the SQL injection.  
`--dbs` tells sqlmap to try enumerating all available databases.  
`--tables` tells sqlmap to try enumerating all available tables.  

It might be smarter to only enumerate the databases first and then enumerate the tables only on the databases you are interested in, because these enumerations take a while.  
However in this approach i have done it both at once.  
We find two databases: `general` and `information_schema`.  
In general, the `information_schema` database is uninteresting for us, and it also contains a ton of tables which is why we maybe only want to iterate through the databases first, and then only look for the tables of the database we are actually interested in.  
The `general` database has two tables: `umfrage-info` and `umfrage`.  
Lets take a look at the `umfrage` table:

```bash
sqlmap -u http://185.244.192.170:20002/index.php --level=5 --risk=3 --method POST --data="topic=mynuke" -p "topic" --dump -D general -T umfrage --dbms=MySQL
```

`-D general` tells sqlmap that we wanna get info about the `general` table.  
`-T umfrage` tells sqlmap that we wanna get info about the `umfrage` column.  

Sadly this only shows us some useless info, so we look at the other table:

```bash
sqlmap -u http://185.244.192.170:20002/index.php --level=5 --risk=3 --method POST --data="topic=mynuke" -p "topic" --dump -D general -T umfrage-infos --dbms=MySQL
```

And there we go, we find our flag :)