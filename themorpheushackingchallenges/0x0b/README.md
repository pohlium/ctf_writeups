# [Challenge 0x0b](https://www.youtube.com/watch?v=YqGNe4Eser4)
In this challenge we are once again given a login page and a shell.  
This time around, the login page actually has some interesting things in it,
but we will look at the source code later.  
However, clicking the "Reset password" link will also tell us to access the
emergency shell, so let's to that.  

Once again the shell IP is not accessible through http, we could now try lots
of different services again but we remember our old friend telnet from the
[Challenge
0x07](https://github.com/fairu1024/TheMorpheusHackingChallenges/tree/master/0x07).  
And indeed, it is a telnet service. However, this time we need to use a proper
interactive shell and not just get some data from the telnet service. We
*could* use python again, but we will just use the linux program `telnet`:

```bash
telnet 185.244.192.170 20012
```

This greets us with an interesting text:

```
Please decrypt the following message to verify that you are the Administrator (itragoadmin@hc.the-morpheus.de)!
Encrypted Message:
-----BEGIN PGP MESSAGE-----

hQEMA/2xjz5/xxcJAQgAm74pvltC8nkzeQobMTLbEMThxpIZg7kL4GUAW1mF+x/I
U3sNwINJ/10qYz1xHrluUghWD67cRAKWanbQvXmMfLKrQbSQQn4s6EGyjRYtQSPE
iAeMFzz39QbK8fnp1IYEpvDtHhC4gFiJ9xCJL92EqVu0YUuKmIOjHHkrUK15/zLX
4uoNr3lQpAOW9O2aAV7+z7Iso2sNdl98FgSl83tNcADVwz0Unw0u0JzCVAJJbEPA
h6BwYAFPNkQleEGa78+hh5CQOPqlrV7mK1k7mR7i5wVBy+K4k1hWtAV1FFRuCXNr
JzvckY39OYV0HJHWb79DCiZ+EjF2OahkMUJPiCtqktJKAT+pf+pOTUxhRERDulgr
AC0oJZrNOpx/rue74ohNUFuW/f9MwUDFfu92ihotIKg3putMTvAA5aP60lTbgDwe
uPkeixwbi3itOpg=
=BwS5
-----END PGP MESSAGE-----
```

You might hope that someone used a very insecure pgp key here, but that's not
the challenge, the key is strong enough to make the message virtually
uncrackable.  
It's now that we look back at the source code of the login page:  
The first interesting thing we see is a request function, that gets callen when
you click on the login button.  
The second interesting thing we see is a getInfo function that seems to send a
POST request to /getInfo so let's try doing that and fiddle around with the
more complex request function later.  

```bash
curl -X POST http://185.244.192.170:20011/getInfo
```

As a response we get a nice pgp keypair, but since we only want to decrypt a
message we are only interested in the private key.  
I believe the response to the servers messages are time bases, and not bound to
a session, so you have to be relatively quick to decrypt the message that was
sent to you via telnet to actually get access to the shell.  
Once you respond with the decrypted message, which should just be a somewhat
long number, you are granted access to the emergency shell.  
A quick `help` shows you a menu of possible actions, with the most powerful one
being an SQL query of your choice.  
At first we want to get an overview of what we can work with, so we do a quick
```sql
SHOW tables;
```
and we will see a couple of tables. Right now, you can still see tables from
old challenges, but there are two tables with "itrago" in their name, so lets
query those:
```sql
SELECT * FROM itrago
```
We see lots of userdata in here, the database has three columns: `ID`,
`username` and `password`.  
Since the challenge has already been solved by people who have their data in
the database now, we actually have a choice to make our life a little easier,
so there are two approaches here.  
### Approach 1
The passwords are obviously hashed, our first goal is to find out what hash has
been used.  
The length of 40 allows us to remove some hashes from the possible algoriths
that could have been used, and one very popular hash with length 40 is sha1.  
Now you might try to simply hash some password in sha1 and put it into the db,
but you will quickly find out that this approach does not work properly.  
It is now that we go back to the source of the login page and look at the
request function a little more in depth:  
Once we get past the boring request object building, we find the login function
that seems to send a POST request to /login.  
A close look at the data reveals that it does'nt actually send our password,
but btoa(password) as a payload.  
This means, that a base64 encoded version of our password gets sent to the
server.  
With this knowledge, we can craft us a nice user to get a valid login:  
I will pick my old credentials again: `fairu1024` as the username, and `12345`
as the password.  
First of all, we base64 encode the password: `MTIzNDU=`  
Now we create the sha1 hash of that string:
`59971a5c6213ecbb4e58bf91b4a56962f05311d8`  
As a little note, at this point we are
still not sure whether the hash is actually sha1 and its just a guess based on
the length we have seen used on the other passwords. While actually solving
this challenge theres a lot of trying out alot of things involved, when
inserting our normal password into the db didnt work it could have easily been
because we chose the wrong hash. The second method thats kinda cheating makes
our life a little easier here.  
But lets insert our credentials into the db now:

```sql
INSERT INTO itrago (username, password) VALUES ("fairu1024", "59971a5c6213ecbb4e58bf91b4a56962f05311d8")
```

If we try to login to the page with our credentials now, we get the flag.  
### Approach 2
We have not yet identified either the hash, nor have we looked at the request
function on the login page and we are currently unaware of the base64 encoding.  
We will just use the data of the previous solvers to make our life a little
easier, as we can expect them to be lazy.  
Just as we did, some of them probably chose easy passwords like 1234 to create
their login, thus, even as base64 encoded versions they are still gonna be in
common hash databases and we will be able to reverse the hash by simply
googling it.  
The hash i used was `ecdec05b479c28bda215eb5430871c275826410d`.  
Googling this hash value reveals the plaintext to be `MTIzNDU2Nzg=` and also
tells us that the hash algorithm used for this was sha1.  
If you have done the previous challenges, spotting that this looks very much
like base64 encoding is easy aswell, so we didnt even have to look at the
sourcecode for this.  
A quick decode shows that the plaintext password is `12345678`.  
We can now use the username linked to the hash and the password we just found
to login and we will also get the flag, without ever investigating the request
function further or the struggle to actually find out the correct hash
algorithm.  
