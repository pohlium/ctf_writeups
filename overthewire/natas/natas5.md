## Login
- username: `natas5`
- password: `Z0NsrtIkJoKALBCLi5eqFfcRN82Au2oD`

## Challenge

We are greeted with the following message:

> Access disallowed. You are not logged in

The page source is not helpful this time either.
However, luckily i still had the networking window open in my browser from the previous challenge.

In there, we see an interesting response HTTP header: `Set-Cookie: loggedin=0`.
You can probably mess with these headers in some way?
I am not sure.
But i do know, that you can use the JavaScript console to change the content of your cookie, so lets go there.

Using the following command, we hopefully get logged in:

`document.cookie='loggedin=1';`

And indeed, after refreshing the page we are now presented the following message:

> Access granted. The password for natas6 is fOIvE0MDtPTgRhqmmvvAOt2EfXR6uQgR
