## Login
- username: `natas1`
- password: `g9D9cREhslqBKtcA2uocGHPfMZVzeFK6`

## Challenge
We are greeted with the message

>  You can find the password for the next level on this page, but rightclicking has been blocked!

Again seems to be a challenge wher you just have to look into the HTML source.
Depending on your browser, the website is either unable to even block it, or you can just view the source by adding a prefix to the URL or through a shortcut.
For firefox at the time of writing, prepending `view-source:` will give you the HTML source, where we again find the next password in a comment.

> The password for natas2 is h4ubbcXrWqsTo7GGnnUMLppXbOogfBZ7
