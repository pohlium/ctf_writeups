## Login
- username: `natas4`
- password: `tKOcJIbzM4lTs8hbCmzn5Zr4434fGZQm`

## Challenge

This site greets us with the message

> Access disallowed. You are visiting from "" while authorized users should come only from "http://natas5.natas.labs.overthewire.org/"

and a link to refresh the page.
Clicking on that link, we see the message changing to the following:

> Access disallowed. You are visiting from "http://natas4.natas.labs.overthewire.org/" while authorized users should come only from "http://natas5.natas.labs.overthewire.org/"

Hmm, interesting.
My initial though was that this might have to do with the `Host` HTTP header, and i went through some hoops to try changing it, but that didn't work out.

However, while doing so, i realized that there was an extra header that was only set when clicking on the "refresh page" link, but not when first visiting the site, a `Referer` header, containing the URL we're coming from.

I'm not sure whether there is a way to easily change that header from within the browser, so i created a little curl command that asks for the website with the correct header for me:

```sh
curl -u natas4:tKOcJIbzM4lTs8hbCmzn5Zr4434fGZQm \
    -H "Referer: http://natas5.natas.labs.overthewire.org/" \
    http://natas4.natas.labs.overthewire.org/index.php
```

This yields us the full HTML source, but this time containing the following message:

> Access granted. The password for natas5 is Z0NsrtIkJoKALBCLi5eqFfcRN82Au2oD
