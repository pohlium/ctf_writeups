## Login
- username: `natas8`
- password: `a6bZCNYwdKqN5cGP11ZdtPg0iImQQhAB`

## Challenge

Again, we are greeted with a webpage with an inpute field, a submit button, and a `View sourcecode` link.

The relevant part of the sourcecode looks like this:

```html
<?

$encodedSecret = "3d3d516343746d4d6d6c315669563362";

function encodeSecret($secret) {
    return bin2hex(strrev(base64_encode($secret)));
}

if(array_key_exists("submit", $_POST)) {
    if(encodeSecret($_POST['secret']) == $encodedSecret) {
    print "Access granted. The password for natas9 is <censored>";
    } else {
    print "Wrong secret";
    }
}
?>

<form method=post>
Input secret: <input name=secret><br>
<input type=submit name=submit>
</form>
```

We can see that the input field is considered as out `secret` POST parameter, and that there is a hardcoded encoded secret `3d3d516343746d4d6d6c315669563362` which is then compared to our provided secret, after that provided secret was passed through the given `encodeSecret` function.

Thus, our goal is to find out what input we have to give to the `encodeSecret` function, such that its output equals the given `encodedSecret` string.

We see that the `encodeSecret` function does three things in this order:

1. base64 encode the raw input
2. reverse the string output of the previous function
3. convert the input ASCII string to hexadecimal representation (the name is a bit misleading, see the [php docs](https://www.php.net/manual/en/function.bin2hex.php))

To find the required input that generates our `encodedSecret` we have to build the inverse function, i.e. reverse the order of operations, as well as the operations themselves (i.e. "ASCII to hex" becomes "hex to ASCII" etc.).
Thus we need a function that does the following:

1. convert a hex string to ASCII
2. reverse the resulting ASCII string
3. base64**de**code the string

I have implemented that algorithm in python:

```python
import base64

def decodeSecret(inp: str) -> str:
    ascii_string = bytes.fromhex(inp).decode('ascii')
    reversed_string = ascii_string[::-1]
    decoded = base64.b64decode(reversed_string).decode('utf-8')
    return decoded

print(decodeSecret("3d3d516343746d4d6d6c315669563362"))
```

This results in the string `oubWYf2kBq`.
Providing this string to the input field of the website yields the result:

```
Access granted. The password for natas9 is Sda6t0vkOPkM8YeOZkAGVhFoaplvlJFd
```
