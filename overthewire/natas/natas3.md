## Login
- username: `natas3`
- password: `G6ctbMJ5Nb4cbFwhpMPSvxGHhQ7I6W8Q`

## Challenge

We are greeted with the message

> There is nothing on this page

The sourcecode contains nothing of interest, except for the following comment:

> No more information leaks!! Not even Google will find it this time...

While this is not a dead giveaway itself, it hints at some kind of mechanism being in place, which prevents google from finding this website.

We know that it is convention to have a file `robots.txt` which contains all the specific paths on your website that you don't want search engines to index (although i am unsure how many crawlers respect that list these days).

And indeed, we find a file at `/robots.txt` with the following comntents:

> User-agent: *
> Disallow: /s3cr3t/

Uh-huh!
Apparently there is a secret directory `/s3cr3t/` on this server.
Going there, we find another file listing, which again contains a file called `users.txt` with the following contents:

> natas4:tKOcJIbzM4lTs8hbCmzn5Zr4434fGZQm
