[Challenge 1](https://www.youtube.com/watch?v=q_4OrvVjWhA)
===========
In the first challenge you just have to perform a GET request and then perform a POST request with
whatever content you get from your GET request as json in the form {"token": content}.
The POST request pattern continues throughout the series, so it wont be explained
anywhere else.
