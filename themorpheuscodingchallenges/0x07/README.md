[Challenge 7](https://www.youtube.com/watch?v=6i92MxAgLsI)
===========
In the seventh challenge we want to find the first pair of integers in a given
list, that when added together results in a given integer k.
The first pair is determined by the smalles index of the two numbers, so if
`min((index1, index2)) < min((index3, index4))` then the pair `(index1, index2)` is considered the first of those two pairs.

