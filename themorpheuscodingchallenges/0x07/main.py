import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/7/"
post_url = "https://cc.the-morpheus.de/solutions/7/"

data = json.loads(requests.get(get_url).text)
k = data["k"]
liste = data["list"]

# we just iterate through the list until we find a matching pair
for i, x in enumerate(liste):
    # the index pair must be two DIFFERENT indice!
    for j, y in enumerate(liste[i+1:], i+1):
        if x + y == k:
            solution = [i, j]
            break
    else:
        continue
    break

payload = {"token": solution}

print(requests.post(post_url, data=json.dumps(payload)).text)
