[Challenge 4](https://www.youtube.com/watch?v=PJaY-KojHe8)
===========
In the fourth challenge we rotate a list k times.
One rotation means to remove the last item of the list, and insert it back to
the front.

### Example:
Source list: `[1, 2, 3, 4]`  
One rotation: `[4, 1, 2, 3]`  
Two rotations: `[3, 4, 1, 2]`  
Three rotations: `[2, 3, 4, 1]`  
Four rotations: `[1, 2, 3, 4]`  
Five rotations: `[4, 1, 2, 3]`  
