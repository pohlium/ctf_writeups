import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/2/"
post_url = "https://cc.the-morpheus.de/solutions/2/"

data = json.loads(requests.get(get_url).text)
k = data["k"]
liste = data["list"]

# find the index of our number
index = liste.index(k)
payload = {"token": index}
print(requests.post(post_url, data=json.dumps(payload)).text)
