[Challenge 3](https://www.youtube.com/watch?v=KhGYv-5leBs)
===========
In the third challenge we want to find the k-th maximum of our list.
We return the k-th biggest number of the list in the usual format.
