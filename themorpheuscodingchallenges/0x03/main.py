import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/3/"
post_url = "https://cc.the-morpheus.de/solutions/3/"

data = json.loads(requests.get(get_url).text)
liste = data["list"]
k = data["k"]

for x in range(k):
    # find current maximum
    maximum = max(liste)
    # remove from list
    liste.remove(maximum)

payload = {"token": maximum}

print(requests.post(post_url, data=json.dumps(payload)).text)

