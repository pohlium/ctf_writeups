import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/14/"
post_url = "https://cc.the-morpheus.de/solutions/14/"

def match(iv1, iv2):
    # iv1 komplett in iv2 enthalten
    if iv1[0] >= iv2[0] and iv1[1] <= iv2[1]:
        return [iv2]
    # iv2 komplett in iv1 enthalten
    if iv1[0] <= iv2[0] and iv1[1] >= iv2[1]:
        return [iv1]
    # iv2 schneidet rechtes ende von iv1
    if iv2[0] <= iv1[1] and iv2[1] >= iv1[1]:
        return [[iv1[0], iv2[1]]]
    # iv2 schneidet linkes ende von iv1
    if iv1[0] <= iv2[1] and iv1[1] >= iv2[1]:
        return [[iv2[0], iv1[1]]]
    return [iv1, iv2]

def merge(intervals):
    had_to_merge = False
    merged_intervals = []
    if len(intervals) > 0:
        merged_intervals.append(intervals.pop())
        while intervals:
            current = intervals.pop()
            for merged_interval in merged_intervals:
                res = match(merged_interval, current)
                if len(res) == 2:
                    continue
                had_to_merge = True
                merged_intervals.remove(merged_interval)
                merged_intervals.append(res.pop())
                break
            else:
                merged_intervals.append(current)
        if had_to_merge:
            merged_intervals = merge(merged_intervals)
    return merged_intervals

data = requests.get(get_url).json()
intervals = data["list"]
print("Starting intervals: ", intervals)
merged_intervals = merge(intervals)
print("Final intervals: ", merged_intervals)

payload = {"token": merged_intervals}
resp = requests.post(post_url, data=json.dumps(payload))
print(resp.text)
