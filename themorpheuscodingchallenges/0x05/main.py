import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/5/"
post_url ="https://cc.the-morpheus.de/solutions/5/"

# create a list of "tokens" that are either a number or an operator
data = requests.get(get_url).text.split(" ")
symbols = ["+", "-", "*", "/"]
# once all operators are removed from the list we are done evaluating
while any(x in data for x in symbols):
    for x in range(len(data)):
        # if the token in position x is an operator
        if data[x] in symbols:
            # calculate the solution of the previous two tokens with the
            # operator x and return the result into the list
            data[x] = str(eval(data[x-2] + data[x] + data[x-1]))
            # remove the previous two tokens
            del(data[x-2])
            del(data[x-2])
            break

payload = {"token": int(eval(data[0]))}
print(requests.post(post_url, data=json.dumps(payload)).text)

