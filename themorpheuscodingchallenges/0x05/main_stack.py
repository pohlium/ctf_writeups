import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/5/"
post_url ="https://cc.the-morpheus.de/solutions/5/"

# create a list of "tokens" that are either a number or an operator
data = requests.get(get_url).text.split(" ")
symbols = ["+", "-", "*", "/"]
# an empty stack to store our results
stack = []
for token in data:
    # if the token is a number we push it to the stack
    if token not in symbols:
        stack.append(token)
    else:
        # we take the previous two numbers from the stack
        prev1 = stack.pop()
        prev2 = stack.pop()
        # and calculate a new number
        stack.append(str(eval(prev2 + token + prev1)))

# remember to cut off your float decimals before submitting!
payload = {"token": int(eval(stack.pop()))}
print(requests.post(post_url, data=json.dumps(payload)).text)

